import { Component, OnInit } from '@angular/core';

import {HttpClient, HttpParams} from '@angular/common/http';
import { Observable } from 'rxjs';
import {ListResponseModel} from '../shared/models/list-response.model';


const GITHUB_URL = 'https://api.github.com/search/repositories';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {
  searchResult$: Observable<ListResponseModel>;

  constructor(
    private http: HttpClient
  ) { }

  ngOnInit() {
  }

  onTextChange(query: string) {
    console.log(query);
    this.searchResult$ = this.fetchRepositories(query);
  }

  private fetchRepositories(query: string): Observable<ListResponseModel> {
    const params = new HttpParams();
    params.set('q', query);
    return this.http.get<ListResponseModel>(GITHUB_URL, { params });
  }

}
