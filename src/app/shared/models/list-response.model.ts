export interface ListResponseModel {
  items: Array<{ name }>;
}
